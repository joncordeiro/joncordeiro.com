---
layout: post
title: Website Redesign
---

Since I launched my site [back in October](/blog/what-ive-learned-while-building-my-first-website) I had been planning on a redesign once I became more familiar with web development. My original design was alright for my first real attempt at web design, but looking back it is easy to see that it could have been much improved.

I felt that the original design was cluttered, so for my redesign I tried to incorporate a lot of whitespace and keep things clean. I moved the navigation from the top of the page to a a fixed sidebar on the left-hand side and removed the link to "Home", instead using my name in the top left as the only means of returning to the home page.

I also went for a more subtle effect when hovering over the navigation links. My old design placed a border underneath the link on hover and shifted content out of position.

![Old colour scheme](/images/blog/oldsitecolorscheme.png)
<p class="caption"> The colour scheme has been greatly improved </p>

Overall, I feel that the redesign is a dramatic improvement over the original. The site is cleaner, easier to read, and easier to navigate. I still need to design a layout for smartphones and Tablets, but I'll put those on the backburner for now to start working on some side-projects and flesh out the ["Projects"](/projects) section of my site.


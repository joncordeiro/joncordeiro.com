---
layout: post
title: Working With Data 140 Million Miles Away
---

Mars is roughly <a href="http://www.space.com/16875-how-far-away-is-mars.html" target="_blank">140 million miles (225 million km) from Earth</a>. That's equivalent to 293 round trips to the Moon, or circumnavigating the globe 5622 times! Despite this immense distance, mankind actually landed their first rover on the Red Planet over 40 years ago! Granted, the mission <a href="http://spectrum.ieee.org/automaton/robotics/industrial-robots/meet-the-very-first-rover-to-land-on-mars" target="_blank">didn't go so well</a>, but the fact that the rover even made it there is impressive. In the 40+ years since, we've landed a handful of rovers on Mars, and they're currently travelling the planet, collecting data, and transmitting it back home to Earth. 

![Curiosity Rover 'selfie'](/images/blog/curiosityselfie.jpg)
<p class="caption"> A 'selfie' taken by NASA's Curiosity rover on Mars. The arm used to take the photo was <a href="http://news.nationalgeographic.com/news/2012/121204-curiosity-mars-rover-portrait-science-space/" target="_blank">edited out</a>.  </p>


The latest rover, NASAs Curiosity, has been collecting and transmitting Mars' atmospheric weather data since 2012. Thanks to the <a href="https://github.com/ingenology/mars_weather_api/" target="_blank">open source {MAAS API}</a> this data is publicly available in a format that's easy to consume and use to build applications. When I discovered this data source I set out to do just that.

It's thanks to this API that I'm excited to reveal my first Android app, <a href="https://play.google.com/store/apps/details?id=com.cordeiro.marsweatherreport" target="_blank">Mars Weather Report</a>.

![Mars Weather Report](/images/blog/MarsWeatherReport.png)
<p class="caption"> From 140 million miles away straight to your phone! </p>

Developing for Android has been a fun challenge. I found the Android APIs easy to work with, and there were more than enough resources out there to help me anywhere I got stuck. I mostly relied on <a href="https://developer.android.com/index.html" target="_blank">Google's own Android Developer site</a> and this <a href="https://github.com/thecodepath/android_guides/wiki" target="_blank">excellent wiki guide</a> from CodePath.

Right now the app is rather simple, only displaying the latest weather reading from Curiosity, but I have some plans to expand upon it. The <a href="https://github.com/ingenology/mars_weather_api/" target="_blank">{MAAS API}</a> provides an archive of all the readings made by the rover, so I hope to add a graphical view of past readings to the app in a future update. I have a couple other ideas lined up, and I'm thinking of building a version for iOS as well.

In the meantime, you can grab the app <a href="https://play.google.com/store/apps/details?id=com.cordeiro.marsweatherreport" target="_blank">here</a>, or from the button below. Be sure to check it out, and if you have any feedback at all, please feel free to contact me on <a href="https://www.twitter.com/JonCordeiro" target="_blank">Twitter</a> or <a href="mailto:contact@joncordeiro.com" target="_blank">send me an email</a>.



<!-- Google Play button -->
<a href="https://play.google.com/store/apps/details?id=com.cordeiro.marsweatherreport">
  <img alt="Get it on Google Play"
       src="https://developer.android.com/images/brand/en_generic_rgb_wo_60.png" />
</a>

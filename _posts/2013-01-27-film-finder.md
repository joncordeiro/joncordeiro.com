---
layout: post
title: Film Finder
github: true
githublink: https://github.com/jcordeiro/FilmFinder
---

Over the past week I've been working on my first little side project and I've built a [film finder](http://filmfinder.joncordeiro.com). The site serves a simple purpose. When you're bored and feel like watching a movie, but you don't know what to watch, simply pick a genre and let the film finder do the rest of the work. A random movie from your genre of choice will be picked and you'll be provided with the film's running time, release date, theatrical poster, a quick overview of the plot, and an embedded YouTube trailer. The poster also acts a link to the movie's page on [themoviedb.org](http://www.themoviedb.org). The site's background image is dynamically changed to reflect the randomly chosen film. If the movie chosen isn't to your liking, finding another is as easy as clicking the "try again" button at the bottom of the page and re-selecting a genre.

![Result page for "The Social Network"](/images/blog/filmfinderscreenshot.png)
<p class="caption"> Result Page </p>

While building this site I made use of the following technologies:

* HTML
* [SASS](http://sass-lang.com)
* The wonderful [Bourbon](http://bourbon.io) library of SASS mixins
* JavaScript
* jQuery
* themoviedb.org's [API](http://docs.themoviedb.apiary.io)

I learned quite a bit while building this site. This was my first time using jQuery, SASS, ajax, and third-party APIs. Using SASS allowed me to make use of the Bourbon library of mixins, which was a great help.

There are a few issues with the site overall. The UI could use some improvement, and it's not as fast as it should be. There are also times when its performance is not up to par. I found that in Opera there were overall performance issues, and the site does not run as fast as it does in other browsers. Another issue is that some of the films have overviews that are much longer than others, which results in the text going further down the page and being overlapped by the movie trailer. Only a small number of films cause this issue though. One other issue is that some of the films' trailers are unavailable on YouTube, they have been taken down for reasons like copyright infringement or account deletion. This issue only occurs for a small number of films.

In the future I hope come back and fix these issues as well as implement some of the following features:

* Allow the user to choose 'random' for their genre
* Optimize layout and performance for mobile devices
* Add link to chosen film on Netflix (if available)

Overall, building this site has served as a valuable learning experience. I've placed the source code on [Github](https://github.com/jcordeiro/FilmFinder) and I encourage you to take a look. I know that that it can definitely be improved so if you have any feedback at all, please feel free to contact me on [Twitter](https://www.twitter.com/JonCordeiro) or [send me an email](mailto:contact@joncordeiro.com).

Thanks!

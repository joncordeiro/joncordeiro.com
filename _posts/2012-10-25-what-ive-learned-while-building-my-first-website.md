---
layout: post
title: What I've Learned While Building My First Website
---

Over the past 2 weeks or so, I set out to educate myself more on web technologies. After spending many hours reading through lots of helpful information from plenty of resources, particularly the [Mozilla Developer Network,](https://developer.mozilla.org/en-US/) I managed to put together this simple website. The site is just static HTML and CSS, and the blog posts are generated by [Jekyll.](http://jekyllrb.com) I've also designed layouts for both smart phones and tablets.

![iPhone Layout](/images/blog/iPhoneLayout.png)
<p class="caption">iPhone Layout</p>

Most of my previous programming experience has been with Java, primarily because it's been the language of choice at both my high school and college. Switching to web development has been quite a change, but the ability to see visual changes from your code instantly is rewarding.

I learned a lot along the way, looking things up as I needed them. Over the course of just a few short weeks, some of the things I've gotten a good grasp on include HTML, CSS, JavaScript, and [Markdown](http://daringfireball.net/projects/markdown/) (which is used to markup these blog posts). I didn't end up using any JavaScript on this site, so to get the hang of the basics I built a [simple to do list](/projects/todo/) web app.

For now, this is just a simple site that's been stretched to the limits of my current HTML, CSS, & JS knowledge. I'll come back and re-write it from the ground up once I have a little more experience. I've already got an idea brewing for my next project, so we'll have to see how it turns out.
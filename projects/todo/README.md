Simple-Todo
===========
A simple todo list web app built using HTML, CSS, and JS.

Clicking the checkboxes alongside the tasks will strikethrough them and gray them out. The new task button will generate a new empty line to enter text for a new task. The clear task button will clear all tasks that have been marked as "completed". If the user clears all tasks, 3 new empty tasks will be generated to prevent the page from being empty.

###Note:

Makes use of jQuery plugin autosize() by Jack Moore http://www.jacklmoore.com/autosize. 

Known Issues
------------
In Firefox when the task is longer than 1 line, an empty scroll bar is placed at the end of the line of text. This only happens for tasks that are added into addition to the intial 3 empty ones. I'm unsure what's causing this. In Opera the task text will not autosize to 2 lines if the length of the text exceeds 1 line. As far as I know, and I've tested, there are no issues when using Safari or Chrome.
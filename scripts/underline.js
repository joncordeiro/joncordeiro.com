$(document).ready(function() {
	
	var current_title = $(document).attr('title');
	
	if (current_title == "Blog") {
		
		$(".blog-link").addClass("current-page");
		
	}
	else if (current_title == "Projects") {
		
		$(".projects-link").addClass("current-page");
		
	}

});